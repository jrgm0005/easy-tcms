<?php

$tourcms;
$channel;

//********************************//
//       SHOW BOOKING             //
//********************************//
function showBooking($bookingId, $operator)
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->show_booking($bookingId, $channel);
    printResponse($response);
}


//********************************//
//       SEARCH VOUCHER           //
//********************************//

function searchVoucher($operator) {
    global $tourcms, $channel;
    operatorInit($operator);
    // super_wide_dates=1
    $redeemFile = file_get_contents("searchVoucher.xml", true);
    $redeemXML = simplexml_load_string($redeemFile);
    $response = $tourcms->search_voucher($redeemXML, $channel);
    printResponse($response);
}

//********************************//
//       REDEEM VOUCHER           //
//********************************//

function redeemVoucher($operator) {
    global $tourcms, $channel;
    operatorInit($operator);
    $redeemFile = file_get_contents("redeemVoucher.xml", true);
    $redeemXML = simplexml_load_string($redeemFile);
    $response = $tourcms->redeem_voucher($redeemXML, $channel);
    printResponse($response);
}

//********************************//
//            SEARCH_TOURS        //
//********************************//
function search_tours($qs = "", $operator = "localhost_142"){
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->search_tours($qs, $channel);
    printResponse($response);
}

//********************************//
//            SEARCH_BOOKINGS     //
//********************************//
function search_bookings($qs = "", $operator = "localhost_142"){
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->search_bookings($qs, $channel);
    printResponse($response);
}

//********************************//
//            CANCEL_BOOKING      //
//********************************//
function cancel_booking($booking_id = 0, $note = "Booking Deleted by testing", $operator = "localhost_142"){
    global $tourcms, $channel;
    operatorInit($operator);
    // Create a new SimpleXMLElement to hold the booking details
    $booking = new SimpleXMLElement('<booking />');
    // Must set the Booking ID on the XML, so TourCMS knows which to cancel
    $booking->addChild('booking_id', $booking_id);
    // Optionally add a note explaining why the booking is cancelled
    $booking->addChild('note', $note);
    // Call TourCMS API, cancelling the booking
    $response = $tourcms->cancel_booking($booking, $channel);
    printResponse($response);
}

//********************************//
//    CHECK_TOUR_AVAILABILITY     //
//********************************//
function check_tour_availability($params, $tour_id, $operator = "localhost_142")
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->check_tour_availability($params, $tour_id, $channel);
    printResponse($response);
}

//********************************//
//       SHOW DEPARTURE           //
//********************************//
function show_departure($departure_id, $tour_id, $operator)
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->show_departure($departure_id, $tour_id, $channel);
    printResponse($response);
}

//********************************//
//            SHOW_TOUR           //
//********************************//
function show_tour($id, $qs = false, $operator = "localhost_142")
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->show_tour($id, $channel, $qs);
    printResponse($response);
}

//********************************//
//            SHOW_CHANNEL        //
//********************************//
function show_channel($operator = "localhost_142")
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->show_channel($channel);
    printResponse($response);
}

//********************************//
//            LIST_CHANNELS       //
//********************************//
// Needed to do as an agent
function test_list_channels($operator = "pos.agent1")
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->list_channels();
    printResponse($response);
}

//********************************//
//            LIST_CHANNELS       //
//********************************//
// Needed to do as an agent
function test_list_tours($operator = "pos.agent1", $channel = 0)
{
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->list_tours();
    printResponse($response);
}

//********************************//
//         CREATE_PAYMENT         //
//********************************//
function create_payment($operator = "localhost_142",$booking_id = 214, $payment_value = 10.00, $payment_currency = "EUR", $payment_type = "Credit Card", $payment_reference = "68ad7c4c9ed211e7812ec5d867e779f7", $gateway_mode = "pos", $payment_transaction_reference = "68ad7c4c9ed211e7812ec5d867e779f7")
{

    global $tourcms, $channel;
    operatorInit($operator);

    // Create a new SimpleXMLElement to hold the payment details
    $payment = new SimpleXMLElement('<payment />');
    $payment->addChild('booking_id', $booking_id);
    $payment->addChild('payment_value', $payment_value);
    $payment->addChild('payment_currency', $payment_currency);
    $payment->addChild('payment_type', $payment_type);
    $payment->addChild('gateway_mode', $gateway_mode);
    $payment->addChild('payment_reference', $payment_reference);
    $payment->addChild('payment_transaction_reference', $payment_transaction_reference);

    $response = $tourcms->create_payment($payment, $channel);
    printResponse($response);
}

//********************************//
//      LIST_PRODUCT_FILTERS      //
//********************************//
function list_product_filters($operator = "localhost_142") {
    global $tourcms, $channel;
    operatorInit($operator);

    $response = $tourcms->list_product_filters($channel);
    printResponse($response);
}

//********************************//
//      LIST_PRODUCT_FILTERS      //
//********************************//
function payworks_booking_payment_new($payment, $operator = "localhost_142") {
    global $tourcms, $channel;
    operatorInit($operator);
    $response = $tourcms->payworks_booking_payment_new($payment, $channel);
    printResponse($response);
}
//********************************//
//            OUTPUT              //
//********************************//
function printResponse($response) {
    print_r($response->asXML());
}
?>
