<?php
require_once('TourCMS.php');
require_once('../functions.php');
// $tourcms = new TourCMS\Utils\TourCMS('40046', '670a74e82621', "simplexml");
// $tourcms = new TourCMS\Utils\TourCMS(0, '0df0db4dc340', "simplexml");
// $tourcms = new TourCMS\Utils\TourCMS(0, '4eeb78887fef', "simplexml");
// $tourcms = new TourCMS\Utils\TourCMS('415443', '6f24ac3ac4ed', "simplexml");
// $tourcms = new TourCMS\Utils\TourCMS(0, '6f24ac3ac4ed', "simplexml");

$tourcms;
$channel;
$my_own_base_url = "192.168.1.41:80/api.tourcms.com";

function operatorInit($operator) {
    $home = $_GET['home'];
    global $tourcms, $channel, $my_own_base_url;
    switch ($operator) {
        case 'localhost_142':
        $tourcms = new TourCMS\Utils\TourCMS(0, '6f24ac3ac4ed', "simplexml");
        $channel = "142";
        break;

        // To log as staff is needed to login using api_login_new and replace the key.
        case 'localhost_142_staff':
        $tourcms = new TourCMS\Utils\TourCMS(0, 'GjX+DXLJzWjrd/uO5hllcUv6WaTateR0ns4NqylzpSI=', "simplexml");
        $channel = "142";
        break;

        case 'localhost_143':
        $tourcms = new TourCMS\Utils\TourCMS(0, '0735845a1560', "simplexml");
        $channel = "143";
        break;
        case 'pos.agent1':
        $tourcms = new TourCMS\Utils\TourCMS(12345, 'ccadca970eea', "simplexml");
        $channel = "142";
        break;
        default:
            # code...
        break;
    }
    // If I am at home, set my own base_url
    if($home == "true")
        $tourcms->set_base_url($my_own_base_url); // HOME URL FOR LOCALHOST
}

//********************************//
//            TESTS               //
//********************************//

// LIST CHANNELS
// test_list_channels();

// SHOW CHANNEL
// show_channel();

// LIST TOURS
// test_list_tours();

// SHOW TOUR
// show_tour(5);

// CHECK_TOUR_AVAILABILITY
// $params = "date=2017-10-10&r1=1";
// $tour_id = 1;
// $operator = "pos.agent1";
// check_tour_availability($params, $tour_id, $operator);

// SEARCH BOOKINGS
// $params = "active=1&per_page=1&page=1";
// search_bookings($params);

// search_tours
// $params = "k=thrid&page=1&per_page=10&404_tour_url=all";
// $operator = "localhost_142";
// search_tours($params, $operator);

// Create payment
// $operator = "localhost_142";
// $booking_id = 214;
// $payment_value = 40.00;
// $payment_currency = "EUR";
// $payment_type = "Credit Card";
// $payment_reference = "689fd76b9f5d11e7a8345598c473a8ca";
// $gateway_mode = "pos";
// $payment_transaction_reference = "689fd76b9f5d11e7a8345598c473a8ca";
// create_payment($operator, $booking_id, $payment_value, $payment_currency, $payment_type, $payment_reference, $gateway_mode, $payment_transaction_reference);

// List_product_filter
list_product_filters();

// payworks_booking_payment_new
// Create a new SimpleXMLElement to hold the payment details
// $payment = new SimpleXMLElement('<payment />');
// $payment->addChild('booking_id', 214);
// $payment->addChild('payworks_payment_reference', "689fd76b9f5d11e7a8345598c473a8ca");
// $payment->addChild('payment_amount', 40.00);
// $payment->addChild('payment_currency', "USD");
// $payment->addChild('payment_type', "Credit Card (Payworks)");
// $payment->addChild('payment_note', "notes");
// $payment->addChild('gateway_mode', "pos");
// $payment->addChild('creditcard_fee_type', 7);
// $payment->addChild('paid_by', "a");
// $payment->addChild('paid_by_id', 7);
// $operator = "localhost_142";
// payworks_booking_payment_new($payment, $operator);

// SHOW DEPARTURE
// $tour_id = 1;
// $departure_id = 409;
// $operator = "localhost_142";
// show_departure($departure_id, $tour_id, $operator);

?>
