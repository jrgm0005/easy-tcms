#### Basic usage of test
php -r "require 'file.php'; functionName(params);"

#### Some samples
php -r "require 'test.php'; redeemVoucher('citySightSeeing');"
php -r "require 'test.php'; search_tours('404_tour_url=all', 'localhost_143');"
php -r "require 'test.php'; search_tours('404_tour_url=all', 'testoperator');"
php -r "require 'test.php'; show_tour(1, 'show_options=1', 'testoperator');"

