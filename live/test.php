<?php

require_once('TourCMS.php');
require_once('../functions.php');

$tourcms;
$channel;

function operatorInit($operator) {
    global $tourcms, $channel;
    switch ($operator)
    {
        case 'testoperator':
        $tourcms = new TourCMS\Utils\TourCMS(0, 'key', "simplexml");
        $channel = "12345";
        break;

        default:
            # code...
        break;
    }
}

//********************************//
//            TESTS               //
//********************************//

// SHOW TOUR
// $tour_id = 29;
// $params = "";
// $operator = "testoperator";
// show_tour($tour_id, $params, $operator);

// SEARCH BOOKINGS
// $params = "active=0&per_page=100";
// $operator = "testoperator";
// search_bookings($params, $operator);

// CANCEL BOOKINGS
// $booking_id = 15566;
// $note = "Booking Deleted by testing";
// $operator = "testoperator";
// cancel_booking($booking_id, $note, $operator);


// CHECK TOUR AVAILABILITY
$params = "date=2017-10-30&r1=1&r2=0&r3=0";
$tour_id = 3;
$operator = "testoperator";
check_tour_availability($params, $tour_id, $operator);


//SEARCH TOURS
// $params = "k=concierge&page=1&per_page=23&404_tour_url=all";
// $params = "id=29&departure_id=103594";
// $operator = "testoperator";
// search_tours($params, $operator);

// SHOW DEPARTURE
// $tour_id = 29;
// $departure_id = 103594;
// $operator = "testoperator";
// show_departure($departure_id, $tour_id, $operator);

?>
